# Postfix


## How to rerun the tasks that update aliases, users, domains

Edit the aliases/users/domains in the ansible vault-encrypted files in `host_vars/secret/`,
then run the playbook using the following tag: `--tags "postfix_update_virtual"`.


## SRS configuration

After installing `postsrsd`, sending any email immediately failed with
`"undelivered mail returned to sender"`.
We appear to be missing our domain in the rewritten envelope:
`<SRS0=Hcj7=I6=mydomain.se=webmaster@>`. Hmm...

It seems the default config for `postsrsd` expects us to explicitly set `SRS_DOMAIN`
in `/etc/default/postsrsd` instead of that line being commented out by default and
`postsrsd` falling back to `postconf -h mydomain`.

+ https://www.besuchet.net/2015/09/srs-configuration-for-postfix-plesk

**Fixed by setting `SRS_DOMAIN`.**

We may want to to also set `SRS_EXCLUDE_DOMAINS=mydomain.se` to avoid SRSing our
own domain (this is not necessary, but I suppose it could be nice).
I have done this, but not seeing any effect on the address envelopes, at least not
as far as I can tell.

There looks like there is more to it, if we care to delve deeper:

+ https://github.com/roehling/postsrsd/issues/14
+ https://github.com/roehling/postsrsd/issues/42
+ https://serverfault.com/questions/706227/only-enable-srs-when-forwarding-to-enable-dmarc
